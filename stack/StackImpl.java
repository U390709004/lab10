package stack;

public class StackImpl implements Stack {
    StackItem top;

    @Override
    public void push(Object item) {
        StackItem oldTop = top;
        top = new StackItem(item);
        top.setNext(oldTop);

    }

    @Override
    public Object pop() {
        if (empty()) throw new RuntimeException("Stack underflow");
        Object item = top.getItem();
        top = top.getNext();
        return item;
    }

    @Override
    public boolean empty() {
        return top == null;
    }
}
